var mysql = require('mysql');
//创建连接池
var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'root',
    database: 'tsgl'
});

//封装SQL操作
function query(sql,params,callback) {
    pool.getConnection(function (err, connection) {
        if (err) throw err;
        console.log("connected success!");
		connection.release();
        connection.query(sql, params, function (err, results, fields) {
            console.log(JSON.stringify(results));
            callback(err, results);
            if (err) throw err;

        });

    });
}

exports.query = query;