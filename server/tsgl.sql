/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 5.5.61-log : Database - tsgl
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`tsgl` /*!40100 DEFAULT CHARACTER SET gbk */;

USE `tsgl`;

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `book_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(20) DEFAULT NULL,
  `book_author` varchar(10) DEFAULT NULL,
  `book_publish` varchar(10) DEFAULT NULL,
  `book_class` varchar(10) DEFAULT NULL,
  `book_exist` int(11) DEFAULT NULL,
  `book_volume` int(8) DEFAULT '0',
  `book_remaining` int(11) DEFAULT NULL,
  `book_del` tinyint(1) DEFAULT '0',
  `book_img` varchar(15) DEFAULT NULL,
  `book_introduct` varchar(500) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=gbk;

/*Data for the table `book` */

insert  into `book`(`book_id`,`book_name`,`book_author`,`book_publish`,`book_class`,`book_exist`,`book_volume`,`book_remaining`,`book_del`,`book_img`,`book_introduct`) values 
(1,'python程序设计','吴惠茹','机械工业出版社','计算机 教育',10,6,4,0,'1.jpg','Python语言是面向对象的模块化设计语言，它易于学习、易于维护、可读性强，适合初学者作为门程序设计语言。本书以Python3.5版本作为教学版，针对初学者的特点，分为五篇内容进行全面讲解：第壹篇为基础入门篇（第1-3章），介绍Python语言的基本功能；第二篇为有序和无序篇（第4-6章），介绍控制台应用程序和窗口应用程序；第三篇为标准函数篇（第7和8章），介绍标准函数库的应用；第四篇为面向对象篇（第9-11章），介绍封装、继承和多态；第五篇为高级篇（第12-15章），介绍Python使用各种模块来处理数据流的高级应用。全书辅以丰富的范例程序和精简的表达方式来降低学习难度，在每个章节的后提供课后练习，提倡读者多动手实践。'),
(2,'JavaScript高级程序设计','马特·弗里斯比','中国工信出版社','计算机 教育',6,4,4,0,'2.jpg','本书是JavaScript经典图书的新版。第4版全面、深入地介绍了JavaScript开发者必须掌握的前端开发技术，涉及JavaScript的基础特性和高级特性。书中详尽讨论了JavaScript的各个方面，从JavaScript的起源开始，逐步讲解到新出现的技术，其中重点介绍ECMAScript和DOM标准。在此基础上，接下来的各章揭示了JavaScript的基本概念，包括类、期约、迭代器、代理，等等。另外，书中深入探讨了客户端检测、事件、动画、表单、错误处理及JSON。本书同时也介绍了近几年来涌现的重要新规范，包括Fetch API、模块、工作者线程、服务线程以及大量新API。'),
(3,'价值','张磊','浙江教育出版社','经济管理 投资理财',4,1,5,0,'3.jpg','高瓴创始人兼首席执行官张磊首部力作,沉淀15年，张磊的投资思想首度全面公开\r\n\r\n全书共3个部分10个章节，介绍了张磊的个人历程、他所坚持的投资理念和方法以及他对价值投资者自我修养的思考，还介绍了他对具有伟大格局观的创业者、创业组织以及对人才、教育、科学观的理解。书中包含大量珍贵资料，不仅有首次提出的7大高瓴公式，详细拆解的10多个投资案例，还有11张精致手绘彩插和19张珍贵照片。\r\n\r\n●高瓴创始人兼首席执行官张磊的的首部力作。\r\n\r\n张磊以乐观主义创业者的心态创立高瓴，坚持价值投资理念，以长期主义和研究驱动发现价值，创造价值，坚定地“重仓中国”，支持实体经济，助力产业创新。经过十五年的发展，高瓴不仅成为亚洲地区资产管理规模最大的投资机构之一，更支持和参与了一批伟大企业的诞生和成长。\r\n\r\n●沉淀15年，张磊的投资思想首度全面公开。\r\n\r\n在这部百万读者翘首以盼的年度巨制中，张磊第一次系统地阐述了他对投资和商业的全方位思考，第一次全面剖析了高瓴的投资体系和创新框架，也第一次倾情分享了他对工作和生活的个人体悟。'),
(4,'自控力','凯利·麦格尼格尔','印刷工业出版社','经济管理',3,6,1,0,'4.jpg','作为一名健康心理学家，凯利•麦格尼格尔博士的工作就是帮助人们管理压力，并在生活中做出积极的改变。多年来，通过观察学生们是如何控制选择的，她意识到，人们关于自控的很多看法实际上妨碍了我们取得成功。例如，把自控力当作一种美德，可能会让初衷良好的目标脱离正轨。所以，麦格尼格尔要求她的学生了解影响自控的生理学基础、心理陷阱和各种社会因素。麦格尼格尔吸收了心理学、神经学和经济学等学科的最新洞见，为斯坦福大学继续教育项目开设了一门叫做“意志力科学”的课程，参与过这门课程的人称其能够“改变一生”。这门课程就是《自控力》一书的基础。本书为读者提供了清晰的框架，讲述了什么是自控力，自控力如何发生作用，以及为何自控力如此重要。'),
(5,'故事','罗伯特·麦基','天津人民出版社','影视原著 艺术设计',2,5,-2,0,'5.jpg','自1997年初版以来，《故事》一直是全世界编剧的第一必读经典，至今，仍属于美国亚马逊最畅销图书中的Top 100。集结了罗伯特•麦基30年的授课经验，本书在对《教父》《阿甘正传》《星球大战》等经典影片的详细分析中，清晰阐述了故事创作的核心原理，其指导意义不应只被影视圈的人所认识，更应得到小说创作、广告策划、文案撰写人才的充分开发。'),
(6,'体验引擎','泰南·西尔维斯特','电子工业出版社','艺术设计 设计',1,0,1,0,'6.jpg','1972年，雅达利公司推出了街机游戏Pong。之后，游戏行业历经了几十年的风风雨雨和几许轮回，依然方兴未艾。多少年以来，无论是俄罗斯方块，还是魔兽世界，游戏制作者面临的挑战都始终如一，即如何为玩家展现出最佳的游戏体验。\r\n\r\n从表面上看，本书的重点在于游戏的设计、规划、平衡性、界面、营销等要素。然而实际上，本书的核心是游戏体验。如何通过游戏设计来创造丰富多彩的游戏体验，以及如何真正从内心打动玩家，才是作者的真正目的。难能可贵的是，本书虽然涉及了许多游戏行业的专业课题，内容却轻松易懂，耐人回味。而读者在阅读本书时，也可以从自己感兴趣的章节开始，逐层深入。所以，无论读者是游戏行业的从业者，还是游戏爱好者，都不妨一读。说不定在某一页，就会不由自主地产生共鸣。'),
(7,'111','222','333','444',5,0,5,1,'7.jpg','111111133333');

/*Table structure for table `borrow` */

DROP TABLE IF EXISTS `borrow`;

CREATE TABLE `borrow` (
  `borrow_id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `borrow_time` date DEFAULT NULL,
  `borrow_ifre` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`borrow_id`),
  KEY `student_id` (`student_id`),
  KEY `book_id` (`book_id`),
  CONSTRAINT `borrow_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`),
  CONSTRAINT `borrow_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=gbk;

/*Data for the table `borrow` */

insert  into `borrow`(`borrow_id`,`book_id`,`student_id`,`borrow_time`,`borrow_ifre`) values 
(1,2,20170001,'2021-02-08',1),
(2,2,20170003,'2021-01-28',0),
(3,1,20170007,'2021-02-26',0),
(4,1,20170001,'2021-02-25',1),
(5,5,20170003,'2021-02-28',1),
(6,2,20170004,'2021-02-28',0),
(7,4,20170001,'2021-02-28',1),
(8,2,20170007,'2021-03-18',0),
(9,4,20170004,'2021-03-19',1),
(10,4,20170004,'2021-05-02',1),
(15,4,20170004,'2021-05-09',1),
(16,4,20170004,'2021-05-09',0);

/*Table structure for table `comment` */

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `comment_id` int(15) NOT NULL AUTO_INCREMENT,
  `comment_content` varchar(50) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `comment_time` datetime DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`comment_id`),
  KEY `book_id` (`book_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`),
  CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=gbk;

/*Data for the table `comment` */

insert  into `comment`(`comment_id`,`comment_content`,`student_id`,`comment_time`,`book_id`) values 
(1,'好',20170001,'2021-02-08 00:00:00',1),
(2,'挺好',20170007,'2021-03-04 00:00:00',1),
(4,'好看',20170004,'2021-03-07 00:00:00',1),
(5,'喜欢',20170007,'2021-03-21 00:00:00',1),
(6,'好书',20170007,'2021-03-21 00:00:00',1),
(7,'强烈安利',20170007,'2021-03-21 00:00:00',1),
(8,'牛啊',20170007,'2021-05-07 00:00:00',1);

/*Table structure for table `commentch` */

DROP TABLE IF EXISTS `commentch`;

CREATE TABLE `commentch` (
  `commentch_id` int(15) NOT NULL AUTO_INCREMENT,
  `commentch_fa` int(15) NOT NULL,
  `commentch_content` varchar(50) DEFAULT NULL,
  `student_id` int(11) DEFAULT NULL,
  `commentch_time` datetime DEFAULT NULL,
  PRIMARY KEY (`commentch_id`),
  KEY `commentch_fa` (`commentch_fa`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `commentch_ibfk_1` FOREIGN KEY (`commentch_fa`) REFERENCES `comment` (`comment_id`),
  CONSTRAINT `commentch_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=gbk;

/*Data for the table `commentch` */

insert  into `commentch`(`commentch_id`,`commentch_fa`,`commentch_content`,`student_id`,`commentch_time`) values 
(1,1,'真是妙蛙种子吃了妙脆角，妙啊',20170004,'2021-03-01 00:00:00'),
(2,1,'好看',20170003,'2021-03-02 00:00:00'),
(3,2,'确实',20170007,'2021-03-21 00:00:00'),
(4,4,'我也觉得',20170007,'2021-03-21 00:00:00'),
(6,1,'好',20170007,'2021-05-07 00:00:00');

/*Table structure for table `manage` */

DROP TABLE IF EXISTS `manage`;

CREATE TABLE `manage` (
  `manage_id` int(11) NOT NULL AUTO_INCREMENT,
  `manage_name` varchar(10) NOT NULL,
  `manage_username` varchar(10) NOT NULL,
  `manage_password` varchar(100) NOT NULL,
  `manage_del` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=gbk;

/*Data for the table `manage` */

insert  into `manage`(`manage_id`,`manage_name`,`manage_username`,`manage_password`,`manage_del`) values 
(1,'张三','manage1','123123',1),
(2,'王五','s54','123321',1),
(3,'李四','ss','123123',1),
(4,'赵五','ss','11111',1),
(5,'柳七','d3','123312',0),
(6,'陈毅','e3','123312',0),
(7,'十四','user1','$2a$10$06LLblkzje8AGNhKSioF3eAfgLYmVHZnnI5A1itlhuogm5kZ81TOa',0),
(8,'十四','user2','$2a$10$YEgIItFrKxtzdiXI1JZjvuEIM8zCDXWBsUdIdYzsioGky5M89jcA.',0);

/*Table structure for table `retur` */

DROP TABLE IF EXISTS `retur`;

CREATE TABLE `retur` (
  `return_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `return_time` date DEFAULT NULL,
  PRIMARY KEY (`return_id`),
  KEY `book_id` (`book_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `retur_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`),
  CONSTRAINT `retur_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=gbk;

/*Data for the table `retur` */

insert  into `retur`(`return_id`,`student_id`,`book_id`,`return_time`) values 
(1,20170001,2,'2021-02-09'),
(2,20170004,2,'2021-02-28'),
(3,20170001,1,'2021-02-28'),
(5,20170004,4,'2021-05-02'),
(6,20170004,4,'2021-05-02'),
(7,20170004,4,'2021-05-02'),
(8,20170004,4,'2021-05-02'),
(9,20170003,3,'2021-05-07'),
(10,20170004,4,'2021-05-09'),
(11,20170004,4,'2021-05-09');

/*Table structure for table `student` */

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `student_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `student_username` varchar(10) DEFAULT NULL COMMENT '账号',
  `student_password` varchar(100) DEFAULT NULL COMMENT '密码',
  `student_name` varchar(10) DEFAULT NULL COMMENT '姓名',
  `student_grade` varchar(10) DEFAULT NULL COMMENT '班级',
  `student_email` varchar(20) DEFAULT NULL COMMENT '邮箱',
  `student_state` tinyint(1) DEFAULT '1' COMMENT '状态',
  `student_del` tinyint(1) DEFAULT '0' COMMENT '是否被删除',
  `student_img` varchar(50) DEFAULT NULL COMMENT '头像地址',
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20170011 DEFAULT CHARSET=gbk;

/*Data for the table `student` */

insert  into `student`(`student_id`,`student_username`,`student_password`,`student_name`,`student_grade`,`student_email`,`student_state`,`student_del`,`student_img`) values 
(20170001,'sdawe4','d878asd','江','信科201803','154845@qq.com',1,1,NULL),
(20170003,'ss','12321','王德发','计科201705','465616@qq.com',1,1,NULL),
(20170004,'sew232','dasd34321','钱','信息201902','21564564@qq.com',1,0,NULL),
(20170005,'dasuhd4','2daw23','钱三','计科201701','454864@qq.com',1,0,NULL),
(20170006,'dawd434','da234','钱七','计科201701','54487554@qq.com',1,0,NULL),
(20170007,'student1','$2a$10$EotHvc/aT5.UBzhE/qiI5OI9PHJNAOm.lRLI4MNd2L08D4C/Iv17y','王','计科201704','',1,0,'20170007.jpg'),
(20170008,'user2','$2a$10$tytknwrBSa.eXcJswYlc6Oz9OyHD38SdR3fQDhcBVjDy96x0Z4Hke','张','信科201802','321321@qq.com',1,1,NULL);

/*Table structure for table `wish` */

DROP TABLE IF EXISTS `wish`;

CREATE TABLE `wish` (
  `wish_id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL,
  `book_name` varchar(20) DEFAULT NULL,
  `book_author` varchar(10) DEFAULT NULL,
  `book_publish` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`wish_id`),
  KEY `student_id` (`student_id`),
  CONSTRAINT `wish_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `student` (`student_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=gbk;

/*Data for the table `wish` */

insert  into `wish`(`wish_id`,`student_id`,`book_name`,`book_author`,`book_publish`) values 
(1,20170007,'货币战争',NULL,NULL),
(2,20170007,'人性的弱点','','');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
