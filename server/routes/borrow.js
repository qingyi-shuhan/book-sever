var express = require('express');
var router = express.Router();
var sd = require('silly-datetime');
var token = require('../model/token')
//导入封装的数据库模块
var mysql = require("../mysql/Config");

//设置跨域访问
router.all("*",function(req,res,next){
    //设置允许跨域的域名，*代表允许任意域名跨域
    res.header("Access-Control-Allow-Origin","*");
    //允许的header类型
    res.header("Access-Control-Allow-Headers","content-type");
    //跨域允许的请求方式 
    res.header("Access-Control-Allow-Methods","DELETE,PUT,POST,GET,OPTIONS");
    if (req.method.toLowerCase() == 'options')
        res.send(200);  //让options尝试请求快速结束
    else
        next();
})


//定义路由

/**
 * @api {Get} /borrow 获取第一页借阅信息
 * @apiGroup borrow
 *
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 *  "count": 1,
 *  "list": [
 *      {
 *          "borrow_id": 1,
 *          "book_name": "JavaScript高级程序设计",
 *          "student_name": "江",
 *          "borrow_time": "2021-02-08",
 *          "borrow_ifre": 0
 *      },
 *      {
 *          "borrow_id": 2,
 *          "book_name": "JavaScript高级程序设计",
 *          "student_name": "王德发",
 *          "borrow_time": "2021-02-07",
 *          "borrow_ifre": 0
 *      }
 *  ]
 * }
 */
router.get('/', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.num];
		let SQL = `SELECT borrow.borrow_id,book.book_name,student.student_name,borrow.borrow_time,borrow_ifre  
		FROM borrow,book,student 
		WHERE borrow.book_id=book.book_id AND borrow.student_id=student.student_id
		limit 0,${param[0]}`
		let cSQL = `SELECT COUNT(*) AS count FROM borrow`
		let count
		mysql.query(cSQL, function (err, results) {
		    if (results) {
				count = Math.ceil(results[0].count/param[0])
				mysql.query(SQL, function (err, results) {
				    if (results) {
						let redata = {
							"count":count,
							"list":results
						}
						redata.list.forEach((obj) => {
							obj.borrow_time = sd.format(obj.borrow_time, 'YYYY-MM-DD')
						})
				        res.json(redata)
				    }
				})
		    }
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
})


/**
 * @api {Get} /borrow/page 按页数获取借阅信息
 * @apiGroup borrow
 *
 * @apiParam {String} condition 条件
 * @apiParam {String} val 内容
 * @apiParam {int} page 页数
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "condition": 'borrow_id'
 *  "val": '1'
 *  "page": 2
 *  "num": 3
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * "list": [
 *      {
 *          "borrow_id": 1,
 *          "book_name": "JavaScript高级程序设计",
 *          "student_name": "江",
 *          "borrow_time": "2021-02-08",
 *          "borrow_ifre": 0
 *      }
 *  ]
 * }
 */
router.get('/page', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.condition, req.query.val, req.query.num,req.query.page];
		let page = (param[3]-1)*param[2]
		let SQL
		if(param[0]==''||param[1]==''){
			SQL = `SELECT borrow.borrow_id,book.book_name,student.student_name,borrow.borrow_time,borrow_ifre  
			FROM borrow,book,student 
			WHERE borrow.book_id=book.book_id AND borrow.student_id=student.student_id
			limit ${page},${param[2]}`
		}else{
			SQL = `SELECT borrow.borrow_id,book.book_name,student.student_name,borrow.borrow_time,borrow_ifre 
			FROM borrow,book,student 
			WHERE borrow.book_id=book.book_id AND borrow.student_id=student.student_id AND ${param[0]} LIKE '%${param[1]}%' 
			limit ${page},${param[2]}`
		}
		mysql.query(SQL, function (err, results) {
		    if (results) {
		        let redata = {
		        	"list":results
		        }
				redata.list.forEach((obj) => {
					obj.borrow_time = sd.format(obj.borrow_time, 'YYYY-MM-DD')
				})
		        res.json(redata)
		    }
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
})



//更新
/**
 * @api {post} /borrow/update 更新借阅信息
 * @apiGroup borrow
 *
 * @apiParam {int} borrow_id 借阅信息编号
 * @apiParam {String} book_id 图书编号
 * @apiParam {int} student_id 学生编号
 * @apiParam {Date} borrow_time 借阅时间
 * @apiParam {bool} borrow_ifre 是否归还
 * @apiParamExample {json} Request-Example
 * {
 *  "borrow_id": 1
 *  "book_id": 1
 *  "student_id": 20170001
 *  "borrow_time": 2021-02-04
 *  "borrow_ifre": 0
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */

router.post('/update', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.body.borrow_id];
		if (req.body.borrow_ifre == '') {
			param.unshift('borrow_ifre')
		} else {
			param.unshift(`${req.body.borrow_ifre}`)
		}
		if (req.body.borrow_time == '') {
			param.unshift('borrow_time')
		} else {
			param.unshift(`'${req.body.borrow_time}'`)
		}
		if (req.body.student_id == '') {
			param.unshift('student_id')
		} else {
			param.unshift(`${req.body.student_id}`)
		}
		if (req.body.book_id == '') {
			param.unshift('book_id')
		} else {
			param.unshift(`${req.body.book_id}`)
			let id
			let cSQL = `SELECT book_id FROM borrow WHERE borrow_id = ${param[4]}`
			mysql.query(cSQL, function (err, results) {
				id = results[0].book_id
			    if (err) console.log("ADD ERROR " + err);
				console.log(id,param[0]);
				let aSQL = `UPDATE book
						set book_remaining=book_remaining+1,book_volume=book_volume-1
						where book_id='${id}'`
				mysql.query(cSQL, function (err, results) {
					console.log('aa');
				    if (err) console.log("ADD ERROR " + err);
				})
				let bSQL = `UPDATE book
						set book_remaining=book_remaining-1,book_volume=book_volume+1
						where book_id='${param[0]}'`
				mysql.query(cSQL, function (err, results) {
					console.log('bb');
				    if (err) console.log("ADD ERROR " + err);
				})
			})
		}
		let SQL = `UPDATE borrow 
		set book_id=${param[0]},student_id=${param[1]},borrow_time=${param[2]},borrow_ifre=${param[3]} 
		where borrow_id=${param[4]}`
		mysql.query(SQL, function (err, results) {
			console.log(results);			
		    res.json()
		    if (err) console.log("DELETE ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
    
})


/**
 * @api {get} /borrow/delete 删除借阅信息
 * @apiGroup borrow
 *
 * @apiParam {int} borrow_id 借阅信息编号
 * @apiParamExample {json} Request-Example
 * {
 *  "borrow_id": 1
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */


router.get('/delete', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.borrow_id];
		let SQL = `DELETE FROM borrow WHERE borrow_id=${param[0]}`
		mysql.query(SQL, function (err, results) {
		    console.log(results);
			
			let cSQL = `UPDATE book
					set book_remaining=book_remaining+1,book_volume=book_volume-1
					where book_id='${param[0]}'`
			mysql.query(cSQL, function (err, results) {
			    if (err) console.log("ADD BOOK ERROR " + err);
			})
			
		    res.json()
		    if (err) console.log("DELETE ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
    
})


/**
 * @api {post} /borrow/add 添加借阅信息
 * @apiGroup borrow
 *
 * @apiParam {int} book_id 图书编号
 * @apiParam {int} student_id 学生编号
 * @apiParamExample {json} Request-Example
 * {
 *  "book_id": 1
 *  "student_id": 20170001
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 * }
 */


router.post('/add', function (req, res) {
	const btoken = req.headers.authorization.split(' ').pop()
	if(token.token.tranToken(btoken)){
		let time = sd.format(new Date(), 'YYYY-MM-DD')
		console.log(req.body);
		let param = [req.body.book_id, req.body.student_id, time];
		let SQL = `INSERT INTO borrow(book_id, student_id, borrow_time) values('${param[0]}',${param[1]},'${param[2]}')`
		mysql.query(SQL, function (err, results) {
		    console.log(results);
			let cSQL = `UPDATE book
					set book_remaining=book_remaining-1,book_volume=book_volume+1
					where book_id='${param[0]}'`
			mysql.query(cSQL, function (err, results) {
			    console.log(results);
				e
			    res.json()
			    if (err) console.log("ADD BOOK ERROR " + err);
			})
		    res.json()
		    if (err) console.log("ADD BOOK ERROR " + err);
		})
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
})

/**
 * @api {Get} /borrow/search 按条件查询借阅信息
 * @apiGroup borrow
 *
 * @apiParam {String} condition 条件
 * @apiParam {String} val 内容
 * @apiParam {int} num 每页数量
 * @apiParamExample {json} Request-Example
 * {
 *  "condition": 'borrow_id'
 *  "val": '1'
 * }
 *
 * @apiSuccessExample  {json} Response-Example
 * {
 *  "count": 1,
 *  "list": [
 *      {
 *          "borrow_id": 1,
 *          "book_name": "JavaScript高级程序设计",
 *          "student_name": "江",
 *          "borrow_time": "2021-02-08",
 *          "borrow_ifre": 0
 *      }
 *  ]
 * }
 */
router.get('/search', function (req, res) {
    const btoken = req.headers.authorization.split(' ').pop()
    if(token.token.tranToken(btoken)){
		let param = [req.query.condition, req.query.val, req.query.num];
		let SQL = `SELECT borrow.borrow_id,book.book_name,student.student_name,borrow.borrow_time,borrow_ifre
		FROM borrow,book,student 
		WHERE borrow.book_id=book.book_id AND borrow.student_id=student.student_id AND borrow.${param[0]} LIKE '%${param[1]}%'
		limit 0,${param[2]}`
		let cSQL = `SELECT COUNT(*) AS count FROM borrow WHERE ${param[0]} LIKE '%${param[1]}%'`
		let count
		mysql.query(cSQL, function (err, results) {
		    if (results) {
				count = Math.ceil(results[0].count/param[2])
				mysql.query(SQL, function (err, results) {
				    if (results) {
						let redata = {
							"count":count,
							"list":results
						}
						redata.list.forEach((obj) => {
							obj.borrow_time = sd.format(obj.borrow_time, 'YYYY-MM-DD')
						})
				        res.json(redata)
				    }
				})
		    }
		})   
	}else{
		res.json({
			status: 422,
			message: "请重新登陆"
		})
	}
	
})



module.exports = router